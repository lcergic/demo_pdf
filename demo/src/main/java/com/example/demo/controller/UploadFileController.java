package com.example.demo.controller;

import com.opencsv.CSVReader;
import com.pdftron.common.PDFNetException;
import com.pdftron.pdf.OCRModule;
import com.pdftron.pdf.OCROptions;
import com.pdftron.pdf.PDFDoc;
import com.pdftron.pdf.PDFNet;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

//import com.opencsv.CSVReader;

//import org.apache.

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
//import org.apache.http.impl.client.HttpClientBuilder;

@CrossOrigin()
@RestController
public class UploadFileController {

    private final static String apiKey = "sl9x4k6u66nm";
    private static List<String> formats = Arrays.asList(new String[] { "csv", "xml", "xlsx-single", "xlsx-multiple" });

    @PostMapping("/uploadForExtraction")
    public ExtractedTable uploadForExtraction(@RequestParam MultipartFile file) throws IOException {
        InputStream inputStream = file.getInputStream();

        String fileName = file.getOriginalFilename();

//        UploadTempImageRequestDto requestInfo = new UploadTempImageRequestDto();
//        requestInfo.setOldTempFileId(oldTempFileId);
//        requestInfo.setFileName(file.getOriginalFilename());
//        requestInfo.setFileContentType(file.getContentType());
//
//        return applicationClient.brandingController.uploadImage(requestInfo, file.getInputStream());

        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpPost httpPost = new HttpPost("https://pdftables.com/api?format=" + formats.get(0).toLowerCase() + "&key=" + apiKey);
//
//
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.addBinaryBody("upstream", inputStream, ContentType.DEFAULT_BINARY, fileName);
//        String objectAsJsonString;
//        try {
//            objectAsJsonString = objectMapper.writeValueAsString(data);
//        } catch (JsonProcessingException exc) {
//            ErrorResponseDto errorResponseDto = new ErrorResponseDto(APPLICATION, ERYCE_UNEXPECTED_SERVER_ERROR, exc.getMessage());
//            throw new MicroserviceException(errorResponseDto);
//        }
//        builder.addTextBody("infoDto", objectAsJsonString, ContentType.APPLICATION_JSON);
//

        httpPost.setEntity(builder.build());
//
        HttpResponse httpResponse;
        try {
            httpResponse = httpClient.execute(httpPost);
        } catch (IOException exc) {
            //?ErrorResponseDto errorResponseDto = new ErrorResponseDto(APPLICATION, ERYCE_UNEXPECTED_SERVER_ERROR, exc.getMessage());
            throw exc;
        }

        InputStream content = httpResponse.getEntity().getContent();

        httpResponse.getEntity().getContent();

        File file2 = new File("GETTINGDATAFROMPDF2.csv");

//        try(OutputStream outputStream = new FileOutputStream(file2)){
//            IOUtils.copy(content, outputStream);
//        } catch (FileNotFoundException e) {
//            // handle exception here
//        } catch (IOException e) {
//            // handle exception here
//        }


        CSVReader csvReader = new CSVReader(new InputStreamReader(content));

        String[] line;
        List<String[]> list = new ArrayList<>();
        boolean headersRead = false;
        ExtractedTable extractedTable = new ExtractedTable();
        while ((line = csvReader.readNext()) != null) {
            if (!headersRead) {
                extractedTable.setHeaders(line);
                headersRead = true;
            } else {
                list.add(line);
            }
        }
        extractedTable.setRows(list);
        csvReader.close();

        return extractedTable;
    }

    class ExtractedTable {
        String[] headers;
        List<String[]> rows;

        public String[] getHeaders() {
            return headers;
        }

        public void setHeaders(String[] headers) {
            this.headers = headers;
        }

        public List<String[]> getRows() {
            return rows;
        }

        public void setRows(List<String[]> rows) {
            this.rows = rows;
        }
    }

    class OCRText {
        String OCRText;

        public String getOCRText() {
            return OCRText;
        }

        public void setOCRText(String OCRText) {
            this.OCRText = OCRText;
        }
    }

    @PostMapping("/uploadForOCR")
    public OCRText uploadForOCR(@RequestParam MultipartFile file) throws IOException, PDFNetException {
        InputStream inputStream = file.getInputStream();

        String fileName = file.getOriginalFilename();

        PDFNet.initialize();

        PDFDoc doc = new PDFDoc(inputStream);
        PDFNet.addResourceSearchPath("./OCRModule");

        OCROptions options = new OCROptions();
        String jsonORCResult = OCRModule.getOCRJsonFromPDF(doc, options);

        System.out.println(jsonORCResult);

        List<String> matches = new ArrayList<String>();
        Matcher m = Pattern.compile("(?=(" + "\"text\":" + "(.*?)" + "," + "))").matcher(jsonORCResult);

        StringBuilder stringBuilder = new StringBuilder();
        while(m.find()) {
            String match = m.group(1);
            match = match.substring(8);
            match = match.replace(",","");
            match = match.replace("\"","");
//            matches.add(match);
            stringBuilder.append(match);
            stringBuilder.append(" \n ");
        }

        OCRText ocrText = new OCRText();
        ocrText.setOCRText(stringBuilder.toString());

        return ocrText;
    }



    @GetMapping("/uploadForExtraction")
    public String stupidTest() {
        return "WTF";
    }
}
